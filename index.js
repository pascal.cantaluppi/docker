var express = require("express");
var app = express();

app.get("/", function(req, res) {
  res.send("I am a docker container!");
});

app.listen(3000, function() {
  console.log("Docker app listening on port 3000!");
});
