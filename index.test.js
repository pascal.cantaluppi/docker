function sum(a, b) {
  return a + b;
}

test("expect sum of 1 and 2 = 3", () => {
  expect(sum(1, 2)).toBe(3);
});
