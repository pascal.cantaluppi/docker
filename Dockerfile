## Test
#FROM node:10 as testing
#WORKDIR /usr/src/app
#COPY package*.json ./
#RUN npm install
#COPY . .
#CMD [ "npm", "run", "test" ]

## Build
FROM node:10
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
#COPY --from=testing . .
COPY . .
EXPOSE 3000
#CMD [ "npm", "run", "start" ]
CMD "node index.js"
